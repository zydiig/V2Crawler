import pymongo

import requests

client = pymongo.MongoClient()
db = client.v2ex
t_col = db.topics
r_col = db.replies
for t in [topic for topic in t_col.find() if len(list(r_col.find({"topic": topic["id"]}))) != topic["replies"]]:
    print(t["id"], t["replies"], len(list(r_col.find({"topic": t["id"]}))))
    # update.fetch_replies(t["id"])
r = requests.get("https://www.v2ex.com/api/replies/show.json?topic_id=302284",
                 headers={'cache-control': 'private, max-age=0, no-cache', "Pragma": "no-cache"})
print(r.text)
print(r.headers["X-Rate-Limit-Remaining"])
print(r.headers)
print(r.request.headers)
for reply in r.json():
    print(reply)
    if r_col.find_one({"id": reply["id"]}):
        print("YES")
    else:
        print("NOOOOOO")
