import pymongo
import tornado.ioloop
import tornado.web


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        client = pymongo.MongoClient()
        db = client.v2ex
        topics = db.topics.find()
        self.render("index.html", topics=topics)


class TopicHandler(tornado.web.RequestHandler):
    def get(self, topic_id):
        client = pymongo.MongoClient()
        db = client.v2ex
        topic = db.topics.find_one({"id": int(topic_id)})
        replies = db.replies.find({"topic": int(topic_id)})
        self.render("topic.html", topic=topic, replies=replies)


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/t/([0-9]+)", TopicHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
