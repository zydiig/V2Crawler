import json
import os
import signal
import time

import pymongo
import requests

import update

print("PID is ", os.getpid())
print("You can send SIGUSR1 to force a refresh.")
client = pymongo.MongoClient()
db = client.v2ex
col = db.topics


def fetch_newest():
    r = requests.get("https://www.v2ex.com/api/topics/latest.json")
    print("Request quota remaining:", r.headers["X-Rate-Limit-Remaining"])
    newest_topics = json.loads(r.text)
    for topic in newest_topics:
        q = col.find_one({"id": topic["id"]})
        if not q:
            update.fetch_new_topic(topic["id"], replies=False, topic=topic)
        else:
            if q["last_touched"] != topic["last_touched"]:
                update.update_topic_metadata(topic["id"], topic_data=topic)
                update.fetch_replies(topic_id=topic["id"])


while True:
    try:
        fetch_newest()
    except KeyboardInterrupt:
        raise KeyboardInterrupt()
    except:
        print("Rate limit exceeded?")
        time.sleep(60)
        continue
    signal.sigtimedwait([signal.SIGUSR1], 120)  # Unix only!
