import datetime
import time

import pymongo

import update

client = pymongo.MongoClient()
db = client.v2ex
t_col = db.topics
while True:
    print("Updating at {}".format(datetime.datetime.now()))
    for topic in t_col.find({"last_touched": {"$lte": time.time() - 7200}}):
        try:
            if update.update_topic_metadata(topic["id"]):
                update.fetch_replies(topic["id"])
        except KeyboardInterrupt:
            raise KeyboardInterrupt
        except:
            print("Fetch failed:", topic["id"], topic["title"])
        time.sleep(15)
    time.sleep(900)
