import pymongo
import requests

client = pymongo.MongoClient()
db = client.v2ex
t_col = db.topics
r_col = db.replies


def fetch_new_topic(topic_id, replies=False, topic=None):
    if not topic:  # Reuse data
        topic = requests.get("https://www.v2ex.com/api/topics/show.json?id={id}".format(id=topic_id)).json()[0]
    t_col.insert({"id": topic["id"],
                  "title": topic["title"],
                  "replies": topic["replies"],
                  "content_rendered": topic["content_rendered"],
                  "member": topic["member"]["id"],
                  "created": topic["created"],
                  "last_touched": topic["last_touched"],
                  "node": topic["node"]["id"]
                  })
    print("Fetched a new topic:", topic["id"], topic["title"])
    if replies:
        fetch_replies(topic_id)


def update_topic_metadata(topic_id, topic_data=None):
    q = t_col.find_one({"id": topic_id})
    if not topic_data:
        topic_data = requests.get("https://www.v2ex.com/api/topics/show.json?id={id}".format(id=q["id"])).json()[0]
    if topic_data["last_touched"] != q["last_touched"]:
        t_col.update_one({"id": topic_id},
                         {"$set": {"replies": topic_data["replies"], "last_touched": topic_data["last_touched"],
                                   "node": topic_data["node"]["id"]}},
                         upsert=False)
        print(
            "Updating topic {id} with {diff} more replies, now at {c}".format(id=topic_id,
                                                                              diff=topic_data["replies"] - q["replies"],
                                                                              c=topic_data["replies"]))
        return True
    else:
        return False


def fetch_replies(topic_id):
    replies = requests.get("https://www.v2ex.com/api/replies/show.json?topic_id={id}".format(id=topic_id)).json()
    for reply in replies:
        if not r_col.find_one({"id": reply["id"]}):
            r_col.insert({"id": reply["id"], "thanks": reply["thanks"], "content_rendered": reply["content_rendered"],
                          "member": reply["member"]["id"], "created": reply["created"], "topic": topic_id})
            print("Inserting a new reply:", repr(reply["content"]), "@/t/{topic}".format(topic=topic_id))
        else:
            pass  # Replies can't be modified.Or can they? Not looking into it now.
